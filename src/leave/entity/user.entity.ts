import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import {Column,Entity, JoinColumn,OneToMany,OneToOne,PrimaryGeneratedColumn,} from 'typeorm';
import { UserLeave } from './leave.entity';
import { UserLogin } from './login.entity';


  @Entity()
  export class User {
  
    @PrimaryGeneratedColumn()
    
    userId: number;

    @ApiProperty({
      type: String,
      description: 'The address of the user',
      default: '',
    })
    @Column()
    @IsString()
    name: string;

    @ApiProperty({
      type: String,
      description: 'The address of the user',
      default: '',
    })
    @Column()
    @IsString()
    password: string;

    @ApiProperty({
      type: String,
      description: 'The address of the user',
      default: '',
    })
    @Column()
    @IsString()
    leaveDate: String;

    @ApiProperty({
      type: Number,
      description: 'The address of the user',
      default: '',
    })
    @Column()
    @IsInt()
    noOfDays: number;

    @OneToOne(() => UserLogin, (login) => login.user, {
      cascade: true,
    })
    login: UserLogin;

  
    @OneToMany(() => UserLeave, (userleave) => userleave.user, {
  
      cascade: true,
  
    })
    @JoinColumn()
    userleave: UserLeave[];
  
    addLeave(userleave: UserLeave) {
  
      if (this.userleave == null) {

        this.userleave = new Array<UserLeave>();
  
      }
  
      this.userleave.push(userleave);
  
    }

    
 
  
  }