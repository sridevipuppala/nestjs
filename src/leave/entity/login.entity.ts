import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class UserLogin {

    @PrimaryGeneratedColumn()
   
    userId: number;

    @ApiProperty({
      type: String,
      description: 'The address of the user',
      default: '',
    })
    @Column()
    @IsString()
    name: string;

    @ApiProperty({
      type: String,
      description: 'The address of the user',
      default: '',
    })
    @Column()
    @IsString()
    password: string;

    @OneToOne(() => User, (user) => user.login, {
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    })
    @JoinColumn()
    user: User;

}








