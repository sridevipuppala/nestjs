import { BadRequestException, Injectable, Logger, Res, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, Repository } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';
import { Response,Request } from 'express';
import { UserLeave } from './entity/leave.entity';
import { User } from './entity/user.entity';
import { UserLogin } from './entity/login.entity';



@Injectable()
export class LeaveService {
  logger:Logger;

  constructor(

    @InjectRepository(UserLeave)
    private readonly leaveRepository: Repository<UserLeave>,
    @InjectRepository(User)
    private readonly dataRepository: Repository<User>,
    private jwtService:JwtService

  ) {
    this.logger=new Logger();
  }

  async getLeaveData(): Promise<User[]> {
    this.logger.log('getAllLeaves is triggered!');
    return await this.dataRepository.find();

  }

  /*async getLeavesById(id: number):Promise<UserLeave> {
    return await this.leaveRepository.findOne(id);

  }*/

  async getLeaveByDate(leavedate:string):Promise<User[]>{
    const details=await this.dataRepository.find({leaveDate:leavedate});
    return details;

   }

  public async applyLeave(newdata: User):Promise<string> {
    const pass = await bcrypt.hash(newdata.password, 10);
    const leavedata = new UserLeave();
    leavedata.totalLeaves = 15;
    leavedata.leavesTaken = newdata.noOfDays;
    leavedata.availableLeaves = leavedata.totalLeaves - leavedata.leavesTaken;
    const userdata = new User();
    const login = new UserLogin();
    userdata.name = login.name=newdata.name;
    userdata.password = login.password = pass;
    userdata.leaveDate = newdata.leaveDate;
    userdata.noOfDays = newdata.noOfDays;
    userdata.login=login;
    userdata.addLeave(leavedata);
    this.dataRepository.save(userdata);
    return `You have successfully applied for ${newdata.noOfDays} days from ${newdata.leaveDate} `;

  }



  async findOne(data: UserLogin,response:Response) {
    const user = await this.dataRepository.findOne({ name: data.name });
    console.log(user);
    if (!user) {
      throw new BadRequestException('invalid credentials');
    }
    if (!(await bcrypt.compare(data.password, user.password))) {
      throw new BadRequestException('invalid ');
    }
    const jwt = await this.jwtService.signAsync({id:user.userId});
    response.cookie('jwt',jwt,{httpOnly:true});
   return{
       message:'success'
   };

  }

  async findUser(request: Request){
    try {
      const cookie= request.cookies['jwt'];
      const data = await this.jwtService.verifyAsync(cookie);
      if(!data) {
        throw new UnauthorizedException();
      }
      const user = await this.dataRepository.findOne({userId: data.id});
      const {password, ...result} = user;
      
      return result;
      } catch(e){
        throw new UnauthorizedException();
      }
  }

  async logout(@Res({passthrough:true}) response:Response){
    response.clearCookie('jwt');
    return {
      message: 'logout'
    }
  }

  async  updateLeaveData( user:User, id:number) {
    await this.dataRepository.update(id,user);
    const leave= new UserLeave();
    leave.totalLeaves = 15;
    leave.leavesTaken = user.noOfDays;
    leave.availableLeaves = leave.totalLeaves - leave.leavesTaken;
    return this.dataRepository.findOne(id);

   }

   updateUser(id: number, date: User) {
    this.dataRepository.update({userId:id},date)
    return 'success'
   }

   updateLeave(id: number, date: UserLeave) {
    this.leaveRepository.update({id:id},date)
    return 'Records successfully updated'
   }
  
  async deleteLeave(id: number) {
    await getConnection().createQueryBuilder().delete().from(UserLeave).where('useruserId= :id',{id:id}).execute();
    await getConnection().createQueryBuilder().delete().from(User).where('userId= :id',{id:id}).execute();
    return 'Deleted leave data succesfully';
  }

}