import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './filters/http.exception.filter';
import { ValidationExceptionFilter } from './filters/validation.exception.filter';
import * as cookieParser from 'cookie-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  //app.useGlobalFilters(new HttpExceptionFilter());
  //app.useGlobalFilters(new ValidationExceptionFilter());
  //app.useGlobalPipes(new ValidationPipe());
  const swaggerConfig=new DocumentBuilder()
  .setTitle('Onetomany example API')
  .setDescription('onetomany-User example CRUD Api')
  .setVersion('1.0')
  .build();

  const doc=SwaggerModule.createDocument(app,swaggerConfig);
  SwaggerModule.setup('api',app,doc);
  app.use(cookieParser())
  app.enableCors({ origin: 'http://localhost:8080', credentials: true })
  await app.listen(3016);
}
bootstrap();