import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LeaveModule } from './leave/leave.module';

@Module({
  imports: [LeaveModule,TypeOrmModule.forRoot({

    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'root',
    database: 'one_many',
    autoLoadEntities: true,
    entities: [LeaveModule],
    synchronize: true
  }),],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
